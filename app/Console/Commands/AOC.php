<?php

namespace App\Console\Commands;

use App\Utilities\Timer;

use Illuminate\Console\Command;
use Ramsey\Uuid\Type\Time;

class AOC extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aoc {starNumber} {argument?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle() {
        $starNumber = $this->argument('starNumber');
        $argument = $this->argument('argument');
        $functionPrefix = 'aoc';
        Timer::stamp();
        $result = $this->{$functionPrefix . $starNumber}($argument);
        Timer::stamp("Finished");
        dump("Result: $result");
    }

    function aoc1() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/1.txt'));
        $rows = explode("\n", $file);
        $outputArray = [];
        foreach ($rows as $row) {
            preg_match_all('/[0-9]/', $row, $matches);
            if (empty($row)) {
                continue;
            }
            $element = $matches[0][0] . end($matches[0]);
            $outputArray[] = $element;
//            dump($row . ": " . $element);
        }
        return array_sum($outputArray);
    }

    function aoc2() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/1.txt'));
        $outputArray = [];
        $numbers = [
            'one' => 1,
            'two' => 2,
            'three' => 3,
            'four' => 4,
            'five' => 5,
            'six' => 6,
            'seven' => 7,
            'eight' => 8,
            'nine' => 9,
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
        ];
        $rows = explode("\n", $file);
        foreach ($rows as $row) {
            if (empty($row)) {
                continue;
            }
            $rowNumbers = [];
            foreach ($numbers as $searchNumber => $numberValue) {
                for ($position = 0; $position > -1; $position++) {
                    $position = strpos($row, $searchNumber, $position);
                    if ($position === false) {
                        break;
                    }
                    $rowNumbers[$position] = $numberValue;
                }
            }
            if (count($rowNumbers) == 0) {
                continue;
            }
            ksort($rowNumbers);
            $rowNumbers = array_values($rowNumbers);
            $element = $rowNumbers[0] . end($rowNumbers);
            $outputArray[] = $element;
//            dump($row . ": " . $element);
        }
        return array_sum($outputArray);
    }

    function aoc3() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/2.txt'));
        $cubes = [
            'red' => 12,
            'green' => 13,
            'blue' => 14
        ];
        $rows = explode("\n", $file);
        $gameIds = [];
        foreach ($rows as $row) {
            if (empty($row)) {
                continue;
            }
            $titleAndGameArray = explode(":", $row);
            $title = $titleAndGameArray[0];
            $gameId = explode(" ", $title)[1];
            $gameIds[$gameId] = $gameId;
            $games = explode(";", $titleAndGameArray[1]);
            foreach ($games as $game) {
                $colors = explode(", ", $game);
                foreach ($colors as $color) {
                    foreach ($cubes as $cube => $value) {
                        if (strpos($color, $cube) !== false) {
                            $amount = intval(str_replace($cube, '', $color));
                            if ($amount > $value) {
                                unset($gameIds[$gameId]);
                                break 3;
                            }
                        }
                    }
                }
            }
        }
        return array_sum($gameIds);
    }

    function aoc4() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/2.txt'));

        $rows = explode("\n", $file);
        $rowAccumulation = [];
        foreach ($rows as $row) {
            if (empty($row)) {
                continue;
            }
            $minAmount = [
                'red' => 0,
                'green' => 0,
                'blue' => 0
            ];
            //Game 1: 1 blue, 8 green; 14 green, 15 blue; 3 green, 9 blue; 8 green, 8 blue, 1 red; 1 red, 9 green, 10 blue
            $gamesString = explode(":", $row)[1];//only take rhs value
            $games = explode(";", $gamesString);//get sets of games
            foreach ($games as $game) {
                $colors = explode(", ", $game);//split game into colors
                foreach ($colors as $color) {
                    foreach ($minAmount as $cube => &$value) {
                        if (strpos($color, $cube) !== false) {//check if cube is in color
                            $amount = intval(str_replace($cube, '', $color));//subtract cube from color and convert to int
                            if ($amount > $value) {
                                $value = $amount;
                            }
                        }
                    }
                }
            }
            $rowAccumulation[] = $minAmount['red'] * $minAmount['green'] * $minAmount['blue'];
        }
        return array_sum($rowAccumulation);
    }

    function findNumbers(string $file, int $width) {
        preg_match_all("/(\d+)/m", $file, $numberMatches, PREG_OFFSET_CAPTURE);
        $numbers = [];
        foreach ($numberMatches[0] as $match) {
            $row = intval(floor($match[1] / $width));
            $column = $match[1] % $width;
            $number = $match[0];
            for ($i = 0; $i < strlen($number); $i++) {
                $numbers[$row][$column + $i] = $number;
            }
        }
        return $numbers;
    }

    function findSymbols(string $file, array $symbols, int $width) {
        preg_match_all("/([\\" . implode('\\', $symbols) . "])/m", $file, $symbolMatches, PREG_OFFSET_CAPTURE);
        $symbols = [];
        foreach ($symbolMatches[0] as $match) {
            $row = intval(floor($match[1] / $width));
            $column = $match[1] % $width;
            $symbols[$row][$column] = $match[0];
        }
        return $symbols;
    }

    function findAttachingNumbersAndAccumulate(array $foundSymbols, array $foundNumbers, \Closure $accumulate) {
        $result = [];
        foreach ($foundSymbols as $symbolRowId => $symbolRow) {
            foreach ($symbolRow as $symbolColumnId => $symbol) {
                $subResults = [];
                for ($rowId = $symbolRowId - 1; $rowId <= $symbolRowId + 1; $rowId++) {
                    for ($columnId = $symbolColumnId - 1; $columnId <= $symbolColumnId + 1; $columnId++) {
                        if (!empty($foundNumbers[$rowId][$columnId])) {
                            $subResults[$foundNumbers[$rowId][$columnId]] = $foundNumbers[$rowId][$columnId];
                        }
                    }
                }
                $accumulate($result, $subResults);
            }
        }
        return array_sum($result);
    }

    function aoc5() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/3.txt'));
        $symbols = [
            '-',
            '&',
            '/',
            '*',
            '@',
            '#',
            '%',
            '+',
            '$',
            '=',
        ];
        $width = 141;
        $foundSymbols = $this->findSymbols($file, $symbols, $width);
        $foundNumbers = $this->findNumbers($file, $width);
        return $this->findAttachingNumbersAndAccumulate($foundSymbols, $foundNumbers, function (&$result, $subResults) {
            $result = array_merge($result, $subResults);
        });
    }

    function aoc6() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/3.txt'));
        $symbols = [
            "\*",
        ];
        $width = 141;
        $foundSymbols = $this->findSymbols($file, $symbols, $width);
        $foundNumbers = $this->findNumbers($file, $width);
        return $this->findAttachingNumbersAndAccumulate($foundSymbols, $foundNumbers, function (&$result, $subResults) {
            if (count($subResults) == 2) {
                $ratio = 1;
                array_walk($subResults, function ($item) use (&$ratio) {
                    $ratio = $ratio * $item;
                });
                $result[] = $ratio;
            }
        });
    }

    function aoc7() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/4.txt'));
        $rows = explode("\n", $file);
        $outputArray = [];
        foreach ($rows as $row) {
            if (empty($row)) {
                continue;
            }
            $bothCardsString = explode(':', $row)[1];
            $bothCardsArray = explode('|', $bothCardsString);
            $winninngCardArray = explode(' ', $bothCardsArray[0]);
            $myCardArray = explode(' ', $bothCardsArray[1]);
            $winningNumbers = [];
            foreach ($winninngCardArray as $winningCardNumber) {
                if (!empty($winningCardNumber) && in_array($winningCardNumber, $myCardArray)) {
//                    dump($winningCardNumber);
                    $winningNumbers[] = $winningCardNumber;
                }
            }
            if (count($winningNumbers) > 0) {
                $outputArray[] = pow(2, count($winningNumbers) - 1);
            }
        }
        return array_sum($outputArray);
    }

    function aoc8() {

        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/4.txt'));
        $rows = explode("\n", $file);
        $cardLibrary = [];
        foreach ($rows as $row) {
            if (empty($row)) {
                continue;
            }
            $totalCard = explode(':', $row);
            $cardTitle = $totalCard[0];
            $cardId = intval(str_replace('Card', '', $cardTitle));
            $bothCardsString = $totalCard[1];
            $bothCardsArray = explode('|', $bothCardsString);
            $winninngCardArray = explode(' ', $bothCardsArray[0]);
            $myCardArray = explode(' ', $bothCardsArray[1]);
            $cardLibrary[$cardId] = [];
            foreach ($winninngCardArray as $winningCardNumber) {
                if (!empty($winningCardNumber)) {
                    $cardLibrary[$cardId][0][] = $winningCardNumber;
                }
            }
            foreach ($myCardArray as $myCardValue) {
                if (!empty($myCardValue)) {
                    $cardLibrary[$cardId][1][] = $myCardValue;
                }
            }
        }
        $myCardsIndex = [];
        foreach ($cardLibrary as $currentIndex => $currentCard) {
            $myCardsIndex[$currentIndex] = 1;
        }
        foreach ($myCardsIndex as $index => &$amount) {
//            dump("Checking index: ".$index);
            $wins = [];
            foreach ($cardLibrary[$index][0] as $winningNumber) {
                if (in_array($winningNumber, $cardLibrary[$index][1])) {
//                        dump("Winning: ".$winningNumber);
                    $wins[] = $winningNumber;
                }
            }
            foreach ($wins as $windex => $win) {
                if (isset($cardLibrary[$index + $windex + 1])) {
//                        dump("Adding: ".$index+$windex+1);
                    $myCardsIndex[$index + $windex + 1] += $amount;
                }
            }
        }
        return array_sum($myCardsIndex);
    }

    function aoc9() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/5.txt'));
        $stringSections = explode("\n\n", $file);
        $seedsSection = array_shift($stringSections);
        $seedsSectionWithoutTitle = trim(explode(":", $seedsSection)[1]);
        $seeds = explode(" ", $seedsSectionWithoutTitle);
        $mappings = [];
        foreach ($stringSections as $stringSection) {

            $sectionMapping = [];
            $sectionWithoutTitle = explode(":\n", $stringSection)[1];
            $sectionRows = explode("\n", $sectionWithoutTitle);
            foreach ($sectionRows as $sectionRow) {
                if (empty($sectionRow)) {
                    continue;
                }
                $explodedRow = explode(" ", $sectionRow);
                $sectionMapping[] = $explodedRow;
            }
            $mappings[] = $sectionMapping;
        }
        $locations = [];
        foreach ($seeds as $seed) {
            $seedResults = [
                intval($seed),
            ];
            foreach ($mappings as $section) {
                $mappedValue = -1;
                $lastValue = last($seedResults);
                foreach ($section as $sectionRows) {
                    $maxRange = $sectionRows[1] + $sectionRows[2];
                    $offset = $sectionRows[0] - $sectionRows[1];
                    if ($lastValue >= $sectionRows[1] && $lastValue < $maxRange) {
                        $mappedValue = intval($lastValue) + $offset;
                        break;
                    }
                }
                if ($mappedValue == -1) {
                    $mappedValue = $lastValue;
                }
                $seedResults[] = $mappedValue;
            }
            $locations[] = last($seedResults);
        }
        return min($locations);
    }

    function isBetween($subject, $minAndInclude, $maxAndInclude) {
        return $subject >= $minAndInclude && $subject < $maxAndInclude;
    }

    function isBetweenInclude($subject, $minAndInclude, $maxAndExclude) {
        return $subject >= $minAndInclude && $subject <= $maxAndExclude;
    }

    function findOverlap($xStart, $xEnd, $yStart, $yEnd) {
        $min = false;
        $max = false;
        if ($this->isBetween($xStart, $yStart, $yEnd)) {
            $min = $xStart;
        }
        if ($this->isBetween($xEnd, $yStart, $yEnd)) {
            $max = $xEnd;
        }
        if ($this->isBetween($yStart, $xStart, $xEnd)) {
            $min = $yStart;
        }
        if ($this->isBetween($yEnd, $xStart, $xEnd)) {
            $max = $yEnd;
        }
        dump("$xStart $xEnd $yStart $yEnd $min $max");
        return [$min, $max];
    }

    function aoc10() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/5-sample.txt'));
        $stringSections = explode("\n\n", $file);
        $seedsSection = array_shift($stringSections);
        $seedsSectionWithoutTitle = trim(explode(":", $seedsSection)[1]);
        $seeds = array_map('intval', explode(" ", $seedsSectionWithoutTitle));
        $mappings = [];
        foreach ($stringSections as $stringSection) {

            $sectionMapping = [];
            $sectionWithoutTitle = explode(":\n", $stringSection)[1];
            $sectionRows = explode("\n", $sectionWithoutTitle);
            foreach ($sectionRows as $sectionRow) {
                if (empty($sectionRow)) {
                    continue;
                }
                $explodedRow = array_map('intval', explode(" ", $sectionRow));
                $explodedRow[2] += $explodedRow[1];
                $explodedRow[0] -= $explodedRow[1];
                $sectionMapping[] = $explodedRow;
            }
            $mappings[] = $sectionMapping;
        }
        $calibratedSeeds = array_chunk($seeds, 2);
        foreach ($calibratedSeeds as &$calibratedSeed) {
            $calibratedSeed[1] += $calibratedSeed[0];
        }
        $lastValues = $calibratedSeeds;
        foreach ($mappings as $sectionIndex => $section) {
            $mappedValues = [];
            //$lastValues contains seeds, then soil>fertilizer>water>light>temp>location, each as a result of previous section
            foreach ($lastValues as $range) {
                $currentMap = [];
                $lowestMin = false;
                $highestMax = false;
                $minRange = $range[0];
                $maxRange = $range[1];
                foreach ($section as $sectionRow) {
//                    dump($sectionRow);
                    $min = false;
                    $max = false;
                    $offset = $sectionRow[0];
                    $minSection = $sectionRow[1];
                    $maxSection = $sectionRow[2];
                    if ($this->isBetween($minRange, $minSection, $maxSection)) {
                        $min = $minRange;
                    }
                    if ($this->isBetween($maxRange, $minSection, $maxSection)) {
                        $max = $maxRange;
                    }
                    if ($this->isBetweenInclude($minSection, $minRange, $maxRange)) {
                        $min = $minSection;
                    }
                    if ($this->isBetweenInclude($maxSection, $minRange, $maxRange)) {
                        $max = $maxSection;
                    }
                    if ($min != false && $max != false) {
                        if(!$lowestMin || $lowestMin > $min) {
                            $lowestMin = $min;
                        }
                        if(!$highestMax || $highestMax < $max) {
                            $highestMax = $max;
                        }
                        $currentMap[] = [
                            $min+$offset,
                            $max+$offset,
                        ];
                    }
                }
                if (count($currentMap)) {
                    if($minRange < $lowestMin) {
                        //Fill before found range
                        $currentMap[] = [
                            $minRange,
                            $lowestMin,
                        ];
                    }
                    if($maxRange > $highestMax) {
                        //Fill after found range
                        $currentMap[] = [
                            $highestMax+1,
                            $maxRange,
                        ];
                    }
                }
                else {
                    //Not found, pass same values for next section
                    $currentMap[] = [
                        $minRange,
                        $maxRange,
                    ];
                }
                $mappedValues = array_merge($mappedValues, $currentMap);
            }

//            dump("Section $sectionIndex", $mappedValues);
            $lastValues = $mappedValues;
        }
        $locations = $lastValues;
        $normalizedLocations = [];
        foreach ($locations as $location) {
            $normalizedLocations[] =$location[0];
        }
//        dump($normalizedLocations);
        $normalizedLocations = array_filter($normalizedLocations);
        return min($normalizedLocations);
    }

    function aoc10alt() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/5-mod.txt'));
        $stringSections = explode("\n\n", $file);
        $seedsSection = array_shift($stringSections);
        $seedsSectionWithoutTitle = trim(explode(":", $seedsSection)[1]);
        $seeds = explode(" ", $seedsSectionWithoutTitle);
        $calibratedSeeds = array_chunk($seeds, 2);
        $mappings = [];
        foreach ($stringSections as $stringSection) {
            $sectionMapping = [];
            $sectionWithoutTitle = explode(":\n", $stringSection)[1];
            $sectionRows = explode("\n", $sectionWithoutTitle);
            foreach ($sectionRows as $sectionRow) {
                if (empty($sectionRow)) {
                    continue;
                }
                $explodedRow = array_map('intval', explode(" ", $sectionRow));
                $explodedRow[2] += $explodedRow[1];
                $explodedRow[0] -= $explodedRow[1];
                $sectionMapping[] = $explodedRow;
            }
            $mappings[] = $sectionMapping;
        }
        $lowestLocation = false;
        foreach ($calibratedSeeds as $seed) {
            for ($i = intval($seed[0]); $i < intval($seed[0] + $seed[1]); $i++) {
                $lastValue = $i;
                foreach ($mappings as $section) {
                    $mappedValue = -1;
                    foreach ($section as $sectionRows) {
                        if ($lastValue >= $sectionRows[1] && $lastValue < $sectionRows[2]) {
                            $mappedValue = intval($lastValue) + $sectionRows[0];
                            break;
                        }
                    }
                    if ($mappedValue == -1) {
                        $mappedValue = $lastValue;
                    }
                    $lastValue = $mappedValue;
                }
                $lastLocation = $lastValue;
                if (!$lowestLocation || $lastLocation < $lowestLocation) {
                    $lowestLocation = $lastLocation;
                }
            }
        }
        return $lowestLocation;
    }

    function aoc11() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/6.txt'));
        $rows = array_filter(explode("\n", $file));
        $inputs = [];
        foreach ($rows as $rowIndex =>$row) {
            $rowInputs = array_values(array_filter(explode(" ", explode(':', $row)[1])));
            foreach ($rowInputs as $index =>$rowInput) {
                $inputs[$index][$rowIndex] = $rowInput;
            }
        }
//        dd($inputs);
        $results = [];
        foreach ($inputs as $input) {
            list($seconds, $distance) = $input;
            $options = $seconds;
            $result = 0;
            for ($i = 1; $i < $options; $i++) {
                if($i * ($options-$i) > $distance) {
                    $result++;
                }
            }
            $results[] = $result;
        }
        $endResult = 1;
        foreach ($results as $result) {
            $endResult *= $result;
        }
        return $endResult;

        /*
         * 7
         * 1-6
         * 2-5
         * 3-4
         * 4-3
         * 5-2
         * 6-1
         *
         * 15
         * 1-14
         * 2-13
         * 3-12
         * 4-11
         * 5-10
         * 6-9
         * 7-8
         * 8-7
         * 9-6
         * 10-5
         * 11-4
         * 12-3
         * 13-2
         * 14-1
         */
    }


    function aoc12() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/6.txt'));
        $rows = array_filter(explode("\n", $file));
        $inputs = [];
        foreach ($rows as $rowIndex =>$row) {
            $rowInputs = array_values(array_filter(explode(" ", explode(':', $row)[1])));
            $inputs[$rowIndex] = (int)implode($rowInputs);
        }
        $inputs = [$inputs];
        $results = [];
        foreach ($inputs as $input) {
            list($seconds, $distance) = $input;
            $options = $seconds-1;
            $startHit = 0;
            $minMiddlepoint = floor($options/2);
            $aggression = 10000;

            $minRange = 1;
            $maxRange = $minMiddlepoint;
            $step = floor($minMiddlepoint / $aggression);
            //Find with large steps
            for ($i = $minRange; $i < $maxRange; $i+=$step) {
                if($i * ($seconds-$i) > $distance) {
                    $startHit = $i-1;
                    break;
                }
            }
            //Find between steps
            $minRange = $startHit - $step;
            $maxRange = $startHit;
            for ($i = $minRange; $i < $maxRange; $i++) {
                if($i * ($seconds-$i) > $distance) {
                    $startHit = $i-1;
                    break;
                }
            }
            $result = ($minMiddlepoint-$startHit)*2;
            if($options % 2 == 1) {
                $result+=1;
            }
            $results[] = $result;
        }
        $endResult = 1;
        foreach ($results as $result) {
            $endResult *= $result;
        }
        return $endResult;
    }

    function aoc13() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/7.txt'));

        $cards = [
            'A' => 'D',
            'K' => 'C',
            'Q' => 'B',
            'J' => 'A',
            'T' => '9',
            '9' => '8',
            '8' => '7',
            '7' => '6',
            '6' => '5',
            '5' => '4',
            '4' => '3',
            '3' => '2',
            '2' => '1',
        ];

        $patterns = [
            [
                2,
            ],
            [
                2,
                2
            ],
            [
                3,
            ],
            [
                3,
                2
            ],
            [
                4,
            ],
            [
                5,
            ],
        ];

        $rows = array_filter(explode("\n", $file));
        $inputs= [];
        foreach ($rows as $row) {
            $columns = explode(" ",$row);
            $hand = $columns[0];
            $bid = intval($columns[1]);
            $type = [];
            foreach ($cards as $cardIndex => $card) {

                $count = substr_count($hand, $cardIndex);
                if($count) {
                    $type[] = $count;
                }
            }
            rsort($type);
            $score = 0;
            foreach (array_reverse($patterns, true) as $index => $pattern) {
                $implodedPattern = implode($pattern);
                $implodedType = implode($type);
                $foundAt = strpos($implodedType, $implodedPattern);
                if($foundAt !== false) {
                    $score += ($index+1);
                    break;
                }
            }
            $handScore = "XXXXX";
            foreach (str_split($hand) as $index => $card) {
                $handScore[$index] = $cards[$card];
            }
            $inputs[] = [
                'bid' => $bid,
                'typescore' => $score,
                'handscore' => $handScore,
//                'hand' => $hand,
            ];
        }
        usort($inputs, function($a, $b) {
            if($a['typescore'] == $b['typescore']) {
                return $a['handscore'] < $b['handscore'] ? -1 : 1;
            }
            return $a['typescore'] < $b['typescore'] ? -1 : 1;
        });
        $output = 0;

        foreach ($inputs as $index => $card) {
//            dump($card['hand']. " -- ".$card['score']);
            $output += ($index+1)*$card['bid'];

        }
//        dd($overview);
        return $output;
    }

    function aoc14() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/7.txt'));

        $cards = [
            'A' => 'D',
            'K' => 'C',
            'Q' => 'B',
            'T' => 'A',
            '9' => '9',
            '8' => '8',
            '7' => '7',
            '6' => '6',
            '5' => '5',
            '4' => '4',
            '3' => '3',
            '2' => '2',
            'J' => '1',
        ];
        $patterns = [
            [
                2,
            ],
            [
                2,
                2
            ],
            [
                3,
            ],
            [
                3,
                2
            ],
            [
                4,
            ],
            [
                5,
            ],
        ];

        $rows = array_filter(explode("\n", $file));
        $inputs= [];
        foreach ($rows as $row) {
            $columns = explode(" ",$row);
            $hand = $columns[0];
            $bid = intval($columns[1]);
            $type = [];
            foreach ($cards as $cardIndex => $card) {

                $count = substr_count($hand, $cardIndex);
                if($cardIndex == "J" && $count != 5) {
                    continue;
                }
                if($count) {
                    $type[] = $count;
                }
            }
            rsort($type);
            $count = substr_count($hand, "J");
            if($type[0] !== 5) {
                $type[0] += $count;
            }
            $score = 0;
            foreach (array_reverse($patterns, true) as $index => $pattern) {
                $implodedPattern = implode($pattern);
                $implodedType = implode($type);
                $foundAt = strpos($implodedType, $implodedPattern);
                if($foundAt !== false) {
                    $score += ($index+1);
                    break;
                }
            }
            $handScore = "XXXXX";
            foreach (str_split($hand) as $index => $card) {
                $handScore[$index] = $cards[$card];
            }
            $inputs[] = [
                'bid' => $bid,
                'typescore' => $score,
                'handscore' => $handScore,
//                'hand' => $hand,
            ];
        }
        usort($inputs, function($a, $b) {
            if($a['typescore'] == $b['typescore']) {
                return $a['handscore'] < $b['handscore'] ? -1 : 1;
            }
            return $a['typescore'] < $b['typescore'] ? -1 : 1;
        });
        $output = 0;

        foreach ($inputs as $index => $card) {
//            dump($card['hand']. " -- ".$card['score']);
            $output += ($index+1)*$card['bid'];

        }
        return $output;

    }

    function aoc15() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/8.txt'));
        $file = str_replace("(", '', $file);
        $file = str_replace(")", '', $file);
        $rows = array_filter(explode("\n", $file));
        $instructions = array_shift($rows);
        $map = [];
        foreach ($rows as $row) {
            $locationsInstructions = explode(" = ", $row);
            $splitLocationInstructions = explode(", ", $locationsInstructions[1]);
            $map[$locationsInstructions[0]] = [
                "L" => $splitLocationInstructions[0],
                "R" => $splitLocationInstructions[1]
            ];
        }
        $currentLocation = "AAA";
        $currentLocationMap = $map[$currentLocation];
        $steps = 0;
        while($currentLocation != "ZZZ") {
            foreach (str_split($instructions) as $instruction) {
                if($currentLocation == "ZZZ") {
                    break;
                }
                else {
//                    $prevLocation = $currentLocation;
                    $currentLocation = $currentLocationMap[$instruction];
                    $currentLocationMap = $map[$currentLocation];
                    $steps++;
                }
            }
        }
       return $steps;
    }
    function aoc16() {

        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/8.txt'));
//        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/8-sample2.txt'));
        $file = str_replace("(", '', $file);
        $file = str_replace(")", '', $file);
        $rows = array_filter(explode("\n", $file));
        $instructions = array_shift($rows);
        $map = [];
        foreach ($rows as $row) {
            $locationsInstructions = explode(" = ", $row);
            $splitLocationInstructions = explode(", ", $locationsInstructions[1]);
            $map[$locationsInstructions[0]] = [
                "L" => $splitLocationInstructions[0],
                "R" => $splitLocationInstructions[1]
            ];
        }
        $startPoints = array_filter(array_keys($map), function ($item) {
            return $item[2] == "A";
        });
        $paths = [];
        foreach ($startPoints as $startPoint) {
            $paths[] = [
                'start' => $startPoint,
                'count' => 0
            ];
        }
        foreach ($paths as &$path) {
            $currentLocation = $path['start'];
            $currentLocationMap = $map[$currentLocation];
            while ($currentLocation[2] != "Z") {
                foreach (str_split($instructions) as $instruction) {
                    if ($currentLocation[2] == "Z") {
                        break;
                    } else {
                        $currentLocation = $currentLocationMap[$instruction];
                        $currentLocationMap = $map[$currentLocation];
                        $path['count']++;
                    }
                }
            }
        }
        $counts = array_column($paths, 'count');
        return array_reduce($counts, function ($a, $b) {
            $modulus = $b;
            $greatestCommonDivisor = $a;
            while ($modulus != 0) {
                $temp = $modulus;
                $modulus = $greatestCommonDivisor % $modulus;
                $greatestCommonDivisor = $temp;
            }
            return ($a * $b) / $greatestCommonDivisor;
        }, 1);
    }

    function aoc17() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/9.txt'));
        $rows = array_filter(explode("\n", $file));
        $inputs = [];
        foreach ($rows as $row) {
            $columns = array_map('intval', explode(" ", $row));
            $inputs[] = [
                'columns' => $columns,
                'length' => count($columns),
                'patterns' => [],
            ];
        }

        foreach ($inputs as &$input) {
            $patterns = [];
            $currentPatterns = $input['columns'];
            $patternLength = $input['length'];
            do {
                $newPattern = [];
                for($i = 1; $i < $patternLength;$i++) {
                    $newPattern[] = $currentPatterns[$i] - $currentPatterns[$i-1];
                }
                $patterns[] = $newPattern;
                $currentPatterns = $newPattern;
                $patternLength--;
            } while(count(array_filter($currentPatterns)) != 0);
            $input['patterns'] = $patterns;
        }
        $outputs = [];
        foreach($inputs as &$input) {
            $patterns = $input['patterns'];
            $patternCount = count($patterns);
            for($i = $patternCount - 1; $i > 0 ; $i--) {
                $lastPatternCount = count($patterns[$i-1]);
                $currentPatternCount = count($patterns[$i]);
                $lastPatternValue = $patterns[$i-1][$lastPatternCount-1];
                $currentPatternValue = $patterns[$i][$currentPatternCount-1];
//                dump(implode(",", $patterns[$i]));
                $patterns[$i-1][] = $lastPatternValue + $currentPatternValue;
            }
            $output = $input['columns'][count($input['columns']) -1] + $patterns[0][count($patterns[0])-1];
            $outputs[] = $output;
            $input['columns'][] = $output;
//            dump(implode(",", $patterns[0]));
//            dump(implode(",", $input['columns']), "");
        }
        return array_sum($outputs);
    }

    function aoc18() {
        $file = file_get_contents(app_path('Console/Commands/AdventOfCode/9.txt'));
        $rows = array_filter(explode("\n", $file));
        $inputs = [];
        foreach ($rows as $row) {
            $columns = array_map('intval', explode(" ", $row));
            $inputs[] = [
                'columns' => $columns,
                'length' => count($columns),
                'patterns' => [],
            ];
        }

        foreach ($inputs as &$input) {
            $patterns = [];
            $currentPatterns = $input['columns'];
            $patternLength = $input['length'];
            do {
                $newPattern = [];
                for($i = 1; $i < $patternLength;$i++) {
                    $newPattern[] = $currentPatterns[$i] - $currentPatterns[$i-1];
                }
                $patterns[] = $newPattern;
                $currentPatterns = $newPattern;
                $patternLength--;
            } while(count(array_filter($currentPatterns)) != 0);
            $input['patterns'] = $patterns;
        }

        $outputs = [];
        foreach($inputs as &$input) {
            $patterns = $input['patterns'];
            $patternCount = count($patterns);
            for($i = $patternCount - 1; $i > 0 ; $i--) {
                $lastPatternValue = $patterns[$i-1][0];
                $currentPatternValue = $patterns[$i][0];
//                dump(implode(",", $patterns[$i]));
                array_unshift($patterns[$i-1],$lastPatternValue - $currentPatternValue);
            }
            $output = $input['columns'][0] - $patterns[0][0];
            $outputs[] = $output;
            array_unshift($input['columns'],$output);
//            dump(implode(",", $patterns[0]));
//            dump(implode(",", $input['columns']), "");
        }
        return array_sum($outputs);
    }
}
