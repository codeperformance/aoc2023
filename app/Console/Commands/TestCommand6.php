<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestCommand6 extends Command {
    protected $signature = 'command:test6';


    protected $description = 'Command description';

//    private $data = [
//        46 => 358,
//        68 => 1054,
//        98 => 1807,
//        66 => 1080
//    ];
//
    private $data = [
        46689866 => 358105418071080
    ];

    /**
     *
     * Execute the console command.*
     * @return int
     */
    public function aoc12alt() {
        $start = microtime(true);
        $result = 0;

        foreach ($this->data as $time => $record) {
            $result = (0 === $result) ? $this->getRootsBasedCount($time, $record) : $this->getRootsBasedCount($time, $record) * $result;
        }

        dd((microtime(true) - $start) * 1000);
        dd($result);
        return Command::SUCCESS;
    }

    private function getRootsBasedCount(int $time, int $record) {

        $b4ac = pow(pow($time, 2) - (4 * $record), 0.5);

        $lowerLimit = ceil(($b4ac - $time) / -2);
        $upperLimit = floor(($time + $b4ac) / 2);

        $lowerLimit = $this->validateLimit($time, $lowerLimit, $record) ? $lowerLimit + 1 : $lowerLimit;
        $upperLimit = $this->validateLimit($time, $upperLimit, $record) ? $upperLimit - 1 : $upperLimit;

        return ($upperLimit - $lowerLimit) + 1;
    }

    private function validateLimit($time, $hold, $record) {
        return (($time - $hold) * $hold == $record);
    }
}
