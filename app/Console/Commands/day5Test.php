<?php



abstract class Timer {
    static $previousTime = 0;
    static $logs = [];
    static function stamp($msg = "") {
        if(self::$previousTime == 0)
        {
            self::$previousTime = microtime(true);
        }
        else {
            $currentTime = microtime(true);
            echo ("$msg: ".(($currentTime-self::$previousTime)*1000)." ms\n");
//            self::$logs[] = "$msg: ".(($currentTime-self::$previousTime)*1000)." ms";
            self::$previousTime = $currentTime;
        }
    }

    static function dump() {
//        dump(self::$logs);
    }
}
function aoc12() {
    $file = file_get_contents(('C:\xampp\htdocs\aoc\app\Console\Commands\AdventOfCode\5-mod.txt'));
    $stringSections = explode("\n\n", $file);
    $seedsSection = array_shift($stringSections);
    $seedsSectionWithoutTitle = trim(explode(":", $seedsSection)[1]);
    $seeds = explode(" ", $seedsSectionWithoutTitle);
    $calibratedSeeds = array_chunk($seeds, 2);
    $mappings = [];
    foreach ($stringSections as $stringSection) {

        $sectionMapping = [];
        $sectionWithoutTitle = explode(":\n", $stringSection)[1];
        $sectionRows = explode("\n", $sectionWithoutTitle);
        foreach ($sectionRows as $sectionRow) {
            if (empty($sectionRow)) {
                continue;
            }
            $explodedRow = array_map('intval', explode(" ", $sectionRow));
            $explodedRow[2] += $explodedRow[1];
            $explodedRow[0] -= $explodedRow[1];
            $sectionMapping[] = $explodedRow;
        }

        $mappings[] = $sectionMapping;
    }
//        Timer::stamp("Finished making mapping");
    $lowestLocation = false;
    foreach ($calibratedSeeds as $seed) {
        for ($i = $seed[0]; $i < $seed[0] + $seed[1]; $i++) {
//                $seedResults = [
//                    intval($i),
//                ];
            $lastValue = $i;
//                Timer::stamp("1");
            foreach ($mappings as $section) {
                $mappedValue = -1;
//                    Timer::stamp("2");
//                    $lastValue = $seedResults[count($seedResults)-1];
//                    Timer::stamp("3");
                foreach ($section as $sectionRows) {
//                        dd($sectionRows);
//                        Timer::stamp("4");
//                        $maxRange = $sectionRows[1] + $sectionRows[2];
////                        Timer::stamp("5");
//                        $offset = $sectionRows[0] - $sectionRows[1];
//                        Timer::stamp("6");
//                        dd($lastValue, $sectionRows, $maxRange);
                    if ($lastValue >= $sectionRows[1] && $lastValue < $sectionRows[2]) {
//                            Timer::stamp("7");
                        $mappedValue = intval($lastValue) + $sectionRows[0];
//                            Timer::stamp("8");
                        break;
                    }
                }
                if ($mappedValue == -1) {
                    $mappedValue = $lastValue;
                }
//                    Timer::stamp("9");
                $lastValue = $mappedValue;
            }
//                Timer::stamp("10");
//                $lastLocation = $seedResults[count($seedResults)-1];
            $lastLocation = $lastValue;
//                Timer::stamp("11");
            if(!$lowestLocation || $lastLocation < $lowestLocation) {
                $lowestLocation = $lastLocation;
            }

//                Timer::stamp("Seed $i loop done");
//                Timer::dump();

        }
    }
    return $lowestLocation;
}



aoc12();
