<?php

namespace App\Utilities;

abstract class Timer {
    static $previousTime = 0;
    static $logs = [];
    static function stamp($msg = "") {
        if(self::$previousTime == 0)
        {
            self::$previousTime = microtime(true);
        }
        else {
            $currentTime = microtime(true);
            if($msg) {
                echo ("$msg: ".(($currentTime-self::$previousTime)*1000)." ms\n");
            }
//            self::$logs[] = "$msg: ".(($currentTime-self::$previousTime)*1000)." ms";
            self::$previousTime = $currentTime;
        }
    }

    static function dump() {
//        dump(self::$logs);
    }
}
